import { NglTestPage } from './app.po';

describe('ngl-test App', () => {
  let page: NglTestPage;

  beforeEach(() => {
    page = new NglTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
