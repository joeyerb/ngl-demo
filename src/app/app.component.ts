import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import { CanvasComponent } from './demos/canvas.component';
import { ModelArgs } from './interfaces/model-args';
import { ModelData } from './interfaces/model-data';
import { DataArgsService } from './demos/data-args';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {

    modelName: string = "spring";
    dataIdx: number = 2;
    rawData: string = null;

    dataSets: Array<string> = [];
    dataSet: string = "cube";

    fileName: string;

    _initialSteps: number = 0;
    currentStep: number = 0;
    intervalID: any;
    playOrPause: string = "Play";

    modelArgs: ModelArgs;
    argsProfiles: Array<string> = [];
    currentArgsProfile: string = "default";

    @ViewChild(CanvasComponent)
    private canvasComponent: CanvasComponent;

    @ViewChild("fileInput")
    private fileInput: ElementRef;

    constructor(private dataArgsService: DataArgsService) {
        this.setDefaultValues();
    }

    /**
     * Set default values
     *
     * @private
     *
     * @memberof AppComponent
     */
    private setDefaultValues(): void {
        this.dataSets = ["666", "888", "cube", "star", "2 cubes"];
        this.modelArgs = this.dataArgsService.getModelArgs("spring");
        this.argsProfiles = this.dataArgsService.getArgsProfiles("spring");
    }

    ngAfterViewInit() {
        this.init();
    }


    /**
     * Initialize app
     *
     * @private
     *
     * @memberof AppComponent
     */
    private init(): void {
        let modelData: ModelData = {
            name: this.modelName,
            idx: this.dataIdx,
            data: this.rawData,
            args: this.modelArgs,
            sets: this.dataSets,
        };
        this.canvasComponent.initRender(modelData);
    }

    /**
     * Change model type
     *
     * @param {any} modelName
     *
     * @memberof AppComponent
     */
    public changeModel(modelName): void {
        this.resetTotalStep();
        this.modelName = modelName;
        this.argsProfiles = this.dataArgsService.getArgsProfiles(this.modelName);
        this.modelArgs = this.dataArgsService.getModelArgs(this.modelName, this.currentArgsProfile);
        this.setArgsProfile();
        this.reset();
    }

    public center(): void {
        this.canvasComponent.resetCamera();
    }

    /**
     * Play
     *
     * @private
     *
     * @memberof AppComponent
     */
    private autoRun(): void {
        this.intervalID = setInterval(this.nextStep.bind(this), this.modelArgs.interval);
        this.playOrPause = "Pause";
    }

    /**
     * Stop (Pause)
     *
     * @private
     *
     * @memberof AppComponent
     */
    private stop(): void {
        if (this.intervalID)
            clearInterval(this.intervalID);
        this.intervalID = null;
        this.playOrPause = "Play";
    }

    /**
     * Play or Pause
     *
     * @memberof AppComponent
     */
    public toggle(): void {
        if (this.intervalID) {
            this.stop();
        } else {
            this.autoRun();
        }
    }

    /**
     * Next
     *
     * @returns {void}
     *
     * @memberof AppComponent
     */
    public nextStep(): void {
        if (this.currentStep >= this.modelArgs.total) {
            this.stop();
            return;
        }
        this.currentStep += 1;
        this.canvasComponent.updateScene('step', this.currentStep);
    }

    /**
     * Reset total step
     *
     * @private
     *
     * @memberof AppComponent
     */
    private resetTotalStep(): void {
        if (this._initialSteps && this._initialSteps > 0) {
            this.modelArgs.total = this._initialSteps;
        }
        this._initialSteps = 0;
    }

    /**
     * Reset
     *
     * @memberof AppComponent
     */
    public reset(): void {
        this.stop();
        this.resetTotalStep();
        this.currentStep = 0;
        let modelData: ModelData = {
            name: this.modelName,
            idx: this.dataIdx,
            data: this.rawData,
            args: this.modelArgs,
            sets: this.dataSets,
        };
        this.canvasComponent.resetScene(modelData);
    }

    /**
     * Result
     *
     * @memberof AppComponent
     */
    public result(): void {
        this.stop();
        if (this._initialSteps === 0) {
            this._initialSteps = this.modelArgs.total;
        }
        this.canvasComponent.updateScene('balance', this.currentStep + 1)
        this.currentStep = this.modelArgs.total;
    }

    /**
     * Change dataset
     *
     * @param {number} idx
     *
     * @memberof AppComponent
     */
    public changeDataset(idx: number): void {
        this.dataIdx = idx;
        this.dataSet = this.dataSets[idx];
        this.fileName = '';
        this.fileInput.nativeElement.value = '';
        this.setArgsProfile();
        this.rawData = null;
        this.reset();
    }

    /**
     * Toggle edges
     *
     * @memberof AppComponent
     */
    public toggleEdges(): void {
        this.canvasComponent.toggleEdges();
    }

    /**
     * Set model's ArgsProfile
     *
     * @private
     *
     * @memberof AppComponent
     */
    private setArgsProfile(): void {
        let datasetName = this.fileName || this.dataSet;
        if (datasetName) {
            this.currentArgsProfile = this.dataArgsService.getArgsProfile(this.modelName, datasetName) ||
                this.currentArgsProfile;
        } else {
            this.currentArgsProfile = 'default';
        }
        this.modelArgs = this.dataArgsService.getModelArgs(this.modelName, this.currentArgsProfile);
    }

    /**
     * Change model's ArgsProfile
     *
     * @param {number} idx
     *
     * @memberof AppComponent
     */
    public changeArgsProfile(idx: number): void {
        this.currentArgsProfile = this.argsProfiles[idx];
        this.modelArgs = this.dataArgsService.getModelArgs(this.modelName, this.currentArgsProfile);
    }

    /**
     * On file change event
     *
     * @param {EventTarget} event
     *
     * @memberof AppComponent
     */
    public onFileChange(event: EventTarget): void {
        let eventObj: MSInputMethodContext = <MSInputMethodContext>event;
        let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
        let files: FileList = target.files;
        let file = files[0];
        this.fileName = file.name;

        let fileReader = new FileReader();
        fileReader.readAsText(file);

        let that = this;
        fileReader.onload = function (e) {
            let rawData = fileReader.result;
            if (rawData)
                that.loadLocalFile(rawData);
        };
    }

    /**
     * Load file data
     *
     * @private
     * @param {string} rawData
     *
     * @memberof AppComponent
     */
    private loadLocalFile(rawData: string): void {
        this.dataIdx = -1;
        this.dataSet = "Custom";
        this.rawData = rawData;
        this.setArgsProfile();
        this.reset();
    }

}
