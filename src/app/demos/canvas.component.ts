import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { WebGLRenderer, Scene, PerspectiveCamera, AxisHelper, GridHelper, PointLight, AmbientLight, DirectionalLight } from 'three';
let TrackballControls = require('three-trackballcontrols');

import { ForceModelDemo } from './force-model.demo';
import { TooltipComponent } from './tooltip.component';
import { ModelData } from '../interfaces/model-data';

@Component({
    selector: 'main-canvas',
    template: '<div #canvasContainer><div id="objcounts">Nodes: {{nodeCount}}, Edges: {{edgeCount}}</div></div> <tooltip></tooltip>',
    styles: ['#objcounts {color: blue; position: absolute; bottom: 0}']
})
export class CanvasComponent implements OnInit {

    renderer: WebGLRenderer;
    scene: Scene;
    camera: PerspectiveCamera;
    controls: any;
    aspectRatio: number;

    nodeCount: number = 0;
    edgeCount: number = 0;

    @ViewChild('canvasContainer')
    private container: ElementRef;

    @ViewChild(TooltipComponent)
    private tooltipComponent: TooltipComponent;

    constructor(
        private forceModelDemo: ForceModelDemo
    ) { }

    ngOnInit() { }

    /**
     * Initialize renderer
     *
     * @param {ModelData} modelData
     *
     * @memberof CanvasComponent
     */
    public initRender(modelData: ModelData): void {

        this.renderer = new WebGLRenderer();
        this.resizeCanvas();

        this.container.nativeElement.appendChild(this.renderer.domElement)

        this.initScene(modelData);

        this.animate();
    }

    /**
     * Initialize Scene
     *
     * @private
     * @param {ModelData} modelData
     *
     * @memberof CanvasComponent
     */
    private initScene(modelData: ModelData): void {
        this.scene = new Scene();
        this.camera = new PerspectiveCamera(75, this.aspectRatio, 1, 100000);
        this.camera.position.z = modelData.args.cameraZ;

        this.tooltipComponent.init(this.scene, this.camera);

        let axisHelper = new AxisHelper(100000);
        this.scene.add(axisHelper);

        let plane = new GridHelper(100000, 100);
        this.scene.add(plane);

        this.initControls();

        // let light = new PointLight(0xffffff, 2, 10000);
        let lighta = new AmbientLight(0x383838)
        this.scene.add(lighta);
        let light = new DirectionalLight(0xffffff);
        light.position.set(0, 0, 8000);
        this.scene.add(light);

        this.renderer.setClearColor(0x2d2d2d);

        this.forceModelDemo.init(modelData, this.scene, this.firstRender.bind(this));
    }

    /**
     * Animate loop
     *
     * @private
     *
     * @memberof CanvasComponent
     */
    private animate(): void {
        requestAnimationFrame(this.animate.bind(this));
        this.tooltipComponent.update();
        this.controls.update();
    }

    /**
     * Initialize controls
     *
     * @private
     *
     * @memberof CanvasComponent
     */
    private initControls(): void {
        this.controls = new TrackballControls(this.camera, this.renderer.domElement);
        this.controls.rotateSpeed = 3.0;
        this.controls.zoomSpeed = 1.2;
        this.controls.panSpeed = 0.8;

        this.controls.noZoom = false;
        this.controls.noPan = false;

        this.controls.staticMoving = true;
        this.controls.dynamicDampingFactor = 0.3;

        this.controls.keys = [65, 83, 68];

        this.controls.addEventListener('change', this.render.bind(this));
    }

    /**
     * Do render
     *
     * @private
     *
     * @memberof CanvasComponent
     */
    private render(): void {
        this.renderer.render(this.scene, this.camera);
    }

    /**
     * Callback for first rendering
     *
     * @private
     * @param {number} [nodeCount]
     * @param {number} [edgeCount]
     *
     * @memberof CanvasComponent
     */
    private firstRender(nodeCount?: number, edgeCount?: number): void {
        this.nodeCount = nodeCount || this.nodeCount;
        this.edgeCount = edgeCount || this.edgeCount;
        this.render();
    }

    /**
     * Update the size of canvas and camera
     *
     * @private
     *
     * @memberof CanvasComponent
     */
    private resizeCanvas(): void {
        let width = window.innerWidth - 24 * 2;
        let height = window.innerHeight - 24 * 4;
        this.renderer.setSize(width, height);

        this.aspectRatio = width / height;
        if (this.camera) {
            this.camera.aspect = this.aspectRatio;
            this.camera.updateProjectionMatrix();
            this.render();
        }
    }

    /**
     * Update scene
     *
     * @param {string} action
     * @param {number} currentStep
     *
     * @memberof CanvasComponent
     */
    public updateScene(action: string, currentStep: number): void {
        this.forceModelDemo.run(this.scene, action, currentStep);
        this.render();
    }

    /**
     * Reset scene
     *
     * @param {ModelData} modelData
     *
     * @memberof CanvasComponent
     */
    public resetScene(modelData: ModelData): void {
        this.scene = null;
        this.renderer.clear();
        this.initScene(modelData);
    }

    /**
     * Reset camera
     *
     * @memberof CanvasComponent
     */
    public resetCamera(): void {
        this.controls.reset();
    }

    /**
     * Toggle edges
     *
     * @memberof CanvasComponent
     */
    public toggleEdges(): void {
        this.forceModelDemo.toggleEdges();
        this.render();
    }

    /**
     * Listener on mousemove
     *
     * @private
     * @param {any} event
     *
     * @memberof CanvasComponent
     */
    @HostListener("mousemove", ['$event'])
    private onMouseMove(event): void {
        this.tooltipComponent.onDocumentMouseMove(event);
    }

    /**
     * Listener on window resize
     *
     * @private
     * @param {any} event
     *
     * @memberof CanvasComponent
     */
    @HostListener("window:resize", ['$event'])
    private onWindowResize(event): void {
        this.resizeCanvas();
    }
}