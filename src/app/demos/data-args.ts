import { Injectable } from '@angular/core';

import { ModelArgs } from '../interfaces/model-args';

@Injectable()
export class DataArgsService {

    private args: any;

    private datasetType: string;

    constructor() {

        let springArgs = {
            "default": () => new ModelArgs(2, 10, 1000, 100, 50),               // tiny
            "far": () => new ModelArgs(2, 10, 1000, 100, 50, null, 15000),      // far
            "small": () => new ModelArgs(4, 2, 500, 10, 40),                    // csv & small
            "middle": () => new ModelArgs(4, 2, 1000, 10, 40),                  // middle
            "normal": () => new ModelArgs(4, 2, 1000, 40, 40, null, 15000),     // middle+
            "slow": () => new ModelArgs(4, 2, 1000, 20, 40, null, 15000),       // middle+
            "large": () => new ModelArgs(4, 4, 1000, 40, 40, null, 15000),      // large
        }

        let frArgs = {
            "default": () => new ModelArgs(0.01, 0.01, 1000, 10, 50),
            "far": () => new ModelArgs(0.01, 0.01, 1000, 10, 50, null, 15000),
            "tiny": () => new ModelArgs(0.001, 0.05, 200, 10, 50, null, 4000),
            "tiny_far": () => new ModelArgs(0.001, 0.05, 200, 10, 50, null, 15000),
            "middle": () => new ModelArgs(0.001, 0.01, 400, 4, 50, null, 10000),
            "large": () => new ModelArgs(0.0005, 0.001, 500, 10, 100, null, 15000),
        }

        let linlogArgs = {
            "default": () => new ModelArgs(0.0000033333, 10000, 0, 10, 50, null, 4000, 0.9, 0.95, 3, 5000000),
            "far": () => new ModelArgs(0.0000033333, 10000, 0, 10, 50, null, 15000, 0.9, 0.95, 3, 5000000),
            "close": () => new ModelArgs(0.000003, 2000, 0, 10, 50, null, 6000, 0.9, 0.95, 3, 5000000),
            "middle": () => new ModelArgs(0.0000004, 400, 0, 40, 50, null, 15000, 0.9, 0.95, 3, 5000000),
            "large": () => new ModelArgs(0.0000002, 1000, 0, 15, 50, null, 10000, 0.9, 0.95, 3, 5000000),
            "large2": () => new ModelArgs(0.0000002, 200, 0, 15, 50, null, 15000, 0.9, 0.95, 3, 5000000),
            "slow": () => new ModelArgs(0.0000002, 50, 0, 20, 50, null, 15000, 0.9, 0.95, 3, 5000000),
        }

        this.args = {
            spring: springArgs,
            fr: frArgs,
            linlog: linlogArgs
        };
    }

    public getArgsProfile(modelName: string, datasetType: string): string {
        this.datasetType = datasetType;
        let profile = 'default';
        if (modelName === 'spring') {
            if (this.contains(['lite4', 'lite5'])) {
                profile = 'middle';
            }
            if (this.startWith(['miserables.', 'd3'])) {
                profile = 'normal';
            }
            if (this.contains(['gd96b', 'gd96d'])) {
                profile = 'slow';
            }
            if (this.startWith(['ar'])) {
                profile = 'slow';
            }
            if (this.contains(['gd96c'])) {
                profile = 'large';
            }
            if (this.startWith(['br.', 'udf.'])) {
                profile = 'far';
            }
        }
        if (modelName === 'fr') {
            if (this.contains(['udf.', 'tsv'])) {
                profile = 'tiny';
            }
            if (this.contains(['miserables', 'gd96b'])) {
                profile = 'middle';
            }
            if (this.contains(['gd96c'])) {
                profile = 'tiny_far';
            }
            if (this.contains(['gd96a'])) {
                profile = 'middle';
            }
            if (this.startWith(['gd96d', 'd3', 'ar'])) {
                profile = 'large';
            }
        }
        if (modelName === 'linlog') {
            if (this.contains(['gd96c', 'tsv'])) {
                profile = 'middle';
            }
            if (this.contains(['miserables'])) {
                profile = 'large';
            }
            if (this.contains(['gd96d', 'd3'])) {
                profile = 'slow';
            }
            if (this.startWith(['gd96b', 'ar'])) {
                profile = 'large2';
            }
            if (this.contains(['udf.'])) {
                profile = 'close';
            }
        }
        return profile;
    }

    public getModelArgs(modelName: string, datasetType?: string): ModelArgs {

        datasetType = datasetType || 'default';

        modelName = this.args[modelName] ? modelName : 'spring';

        return this.args[modelName][datasetType] ? this.args[modelName][datasetType]() : this.args[modelName].default();
    }

    public getArgsProfiles(modelName: string): Array<string> {
        return Object.keys(this.args[modelName] || this.args.spring);
    }

    private startWith(names: Array<string>) {
        return names.find(name => this.datasetType.startsWith(name));
    }

    private contains(names: Array<string>) {
        return names.find(name => this.datasetType.indexOf(name) >= 0);
    }
}
