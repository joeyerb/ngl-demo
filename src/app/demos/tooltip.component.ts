import { Component } from '@angular/core';
import { Vector3, Raycaster, Scene, Camera } from 'three';

@Component({
    selector: 'tooltip',
    template: '<div *ngIf="tipSpan.show" [style.left]="tipSpan.left" [style.top]="tipSpan.top" [innerHTML]="message"></div>',
    styles: ['div {border-radius: 8px; position: absolute; background: white; padding: 0 8px;}'],
})
export class TooltipComponent {

    private mouse: any = { x: 1, y: 1, };
    private scene: Scene;
    private camera: Camera;

    private message: string = "";

    private INTERSECTED: any;
    private vertices: Array<any> = [];

    public tipSpan: any = {
        show: false,
        left: "0px",
        top: "0px",
    }

    constructor() { }


    /**
     * Initialization
     *
     * @param {Scene} scene
     * @param {Camera} camera
     *
     * @memberof TooltipComponent
     */
    public init(scene: Scene, camera: Camera): void {
        this.scene = scene;
        this.camera = camera;
        this.vertices = [];
    }

    /**
     * Update position on mousemove
     *
     * @param {any} event
     *
     * @memberof TooltipComponent
     */
    public onDocumentMouseMove(event): void {
        // update the mouse variable
        this.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        this.mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;

        // update tooltip position
        this.tipSpan.left = event.clientX + "px";
        this.tipSpan.top = event.clientY + "px";
    }


    /**
     * Update scene
     *
     * @memberof TooltipComponent
     */
    public update(): void {
        // create a Ray with origin at the mouse position
        //   and direction into the scene (camera direction)
        let ray = new Raycaster();
        ray.setFromCamera(this.mouse, this.camera);

        // Add all nodes to an array (this.vertices) with which the ray intersects
        if (this.vertices.length === 0) {
            this.scene.traverse(obj => {
                if (obj.name) {
                    if (obj.name.indexOf('v') === 0) {
                        this.vertices.push(obj);
                    }
                }
            });
        }
        let intersects = ray.intersectObjects(this.vertices);

        // INTERSECTED: the object in the scene currently closest to the camera
        //      and intersected by the Ray projected from the mouse position

        // if there is one (or more) intersections
        if (intersects.length > 0) {
            // if the closest object intersected is not the currently stored intersection object
            if (intersects[0].object != this.INTERSECTED) {
                // update tooltip text
                let obj = intersects[0].object;
                if (obj.userData) {
                    this.message = obj.userData.name ? "<h3>" + obj.userData.name + "</h3>" : "";
                    this.message += obj.userData.title ? "<p>" + obj.userData.title + "</p>" : "";
                    this.message += obj.userData.description ? "<p>" + obj.userData.description + "</p>" : "";
                }
                if (!this.message && obj.name)
                    this.message = obj.name.substr(1);
                this.tipSpan.show = true;
            }
        }
        else // there are no intersections
        {
            // remove previous intersection object reference
            //     by setting current intersection object to "nothing"
            this.INTERSECTED = null;
            this.tipSpan.show = false;
        }

    }

}