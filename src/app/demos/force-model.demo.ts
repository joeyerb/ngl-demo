import { Injectable } from '@angular/core';
import { Vector3, Scene } from 'three';
import { ForceModel } from '../interfaces/force-model';
import { SpringModel } from '../models/spring.model';
import { FRModel } from '../models/fr.model';
import { LinLogModel } from '../models/linlog.model';
import { ObjectsService } from '../layout/objects.service';
import { RenderService } from '../layout/render.service';
import { GraphDataset } from '../interfaces/graph-dataset';
import { Graph } from '../interfaces/graph';
import { ModelData } from '../interfaces/model-data';

import * as ds_1 from '../datasets/1.fg.eades-1a';
import * as ds_2 from '../datasets/2.ge.octahedron';
import * as ds_3 from '../datasets/3.ge.cube';
import * as ds_4 from '../datasets/4.ge.star41';
import * as ds_5 from '../datasets/5.ge.2cubes';
import { DataReader } from '../datasets/0.data.reader';
import { TsvReader } from '../datasets/6.tsv.reader';
import { JsonReader } from '../datasets/7.json.reader';
import { CsvReader } from '../datasets/8.csv.reader';

@Injectable()
export class ForceModelDemo {

    constructor(
        private objectsService: ObjectsService,
        private renderService: RenderService,
        private dataReader: DataReader,
        private jsonReader: JsonReader,
        private tsvReader: TsvReader,
        private csvReader: CsvReader) {
            this.fillDatasets();
        }

    private datasets: Array<any>;
    private appDatasets: Array<string>;

    private forceModel: ForceModel;
    private graph: Graph;

    private fillDatasets(): void {
        this.datasets = [];
        this.datasets.push(ds_1.dataset);
        this.datasets.push(ds_2.dataset);
        this.datasets.push(ds_3.dataset);
        this.datasets.push(ds_4.dataset);
        this.datasets.push(ds_5.dataset);
        this.dataReader.getIndex(this.loadOnlineDatasets.bind(this));
    }

    private loadOnlineDatasets(fileNames: Array<string>): void {
        fileNames.forEach(fileName => {
            this.datasets.push({ graph: -1, url: './datasets/' + fileName, name: fileName });
        });
        this.checkAppDatasets();
    }

    private checkAppDatasets(): void {
        if (this.appDatasets.length !== this.datasets.length) {
            this.appDatasets.length = 0;
            this.appDatasets.push(...(this.datasets.map(data => data.name || 'unknown')));
        }
    }

    public init(modelData: ModelData, scene: any, callback: Function): void {

        if (!modelData.name) return;

        switch (modelData.name) {
            case 'spring':
                this.forceModel = new SpringModel();
                break;
            case 'fr':
                this.forceModel = new FRModel();
                break;
            case 'linlog':
                this.forceModel = new LinLogModel();
                break;
        }

        this.forceModel.setArgs(modelData.args);

        if (modelData.sets) {
            this.appDatasets = modelData.sets;
            this.checkAppDatasets();
        }

        if (modelData.data) {
            // 1. Uploaded file
            if (modelData.data[0] === '{') {
                this.jsonReader.getData(modelData.data, (graph: Graph) => {
                    this.firstRun(graph, scene, callback);
                });
            } else if (modelData.data.indexOf('\t') >= 0) {
                this.tsvReader.getData(modelData.data, (graph: Graph) => {
                    this.firstRun(graph, scene, callback);
                });
            } else {
                this.csvReader.getData(modelData.data, (graph: Graph) => {
                    this.firstRun(graph, scene, callback);
                });
            }
        } else {

            let data = this.datasets[modelData.idx || 0];

            if (data.connections) {
                // 2. Test datasets
                let graph = data.toGraph();
                this.firstRun(graph, scene, callback);
            } else if (data.url) {
                // 3. Online data
                if (data.url.endsWith('.tsv')) {
                    this.tsvReader.getHttpData(data.url, (graph: Graph) => {
                        this.firstRun(graph, scene, callback);
                    });
                } else
                if (data.url.endsWith('.csv')) {
                    this.csvReader.getHttpData(data.url, (graph: Graph) => {
                        this.firstRun(graph, scene, callback);
                    });
                } else
                if (data.url.endsWith('.json')) {
                    this.jsonReader.getHttpData(data.url, (graph: Graph) => {
                        this.firstRun(graph, scene, callback);
                    });
                }
            } else {
                // Error
                return;
            }
        }
    }

    public run(scene: any, action: string, currentStep: number) {

        this.renderService.init(this.forceModel, this.graph, scene);

        if (action === 'balance') {
            this.renderService.balance(currentStep);
        } else if (action === 'step') {
            this.renderService.step(1, currentStep);
        } else {
            this.renderService.render();
        }
    }

    private firstRun(graph: Graph, scene: Scene, callback: Function) {
        this.graph = graph;
        let nodeCount = this.graph.vertices.length;
        let edgeCount = this.graph.edges.reduce((count, row) => count + row.length - 1, 0) / 2;
        if (scene) this.run.call(this, scene, null, 1);
        typeof callback === 'function' && callback(nodeCount, edgeCount);
    }

    public toggleEdges(): void {
        this.renderService.toggleEdges();
    }
}
