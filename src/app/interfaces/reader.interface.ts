export interface DataReaderInterface {
    getData: Function;
    loadResp: Function;
}