import { Vector3 } from 'three';
import { NamedPoint } from './named-point';

export interface NamedLink {
    source: NamedPoint | Vector3;
    target: NamedPoint | Vector3;
    name: string;
    idx?: number;
    sourceName?: string;
    targetName?: string;
    description?: string;
    weight?: number;
}