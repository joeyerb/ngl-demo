import { Vector3 } from 'three';
import { NamedPoint } from './named-point';
import { NamedLink } from './named-link';

export interface Graph {
    vertices: Array<NamedPoint>;
    edges: Array<Array<NamedPoint>>;
    groups?: any;
}