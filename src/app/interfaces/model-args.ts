export class ModelArgs {
    attr: number;       // c1 引力参数
    repu: number;       // c3 斥力参数
    slen: number;       // c2 弹簧原长
    mlen: number;       // c4 位移参数
    total: number;      // c5 总步数
    interval?: number;  // 播放间隔
    cameraZ?: number;   // 相机位置
    time1?: number;     // 高阶比例
    time2?: number;     // 一阶比例
    rstart?: number;    // 初始阶数
    scale?: number;

    constructor(attr, repu, slen, mlen, total, interval?, cameraZ?, time1?, time2?, rstart?, scale?) {
        this.attr = attr;
        this.repu = repu;
        this.slen = slen;
        this.mlen = mlen;
        this.total = total;

        this.interval = interval || 100;
        this.cameraZ = cameraZ || 4000;

        // LinLog
        this.time1 = time1 || 0;
        this.time2 = time2 || 0;
        this.rstart = rstart || 0;
        this.scale = scale || 1;
    }
}