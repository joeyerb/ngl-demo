import { ModelArgs } from './model-args';

export interface ModelData {
    idx: number;
    data: string;
    name: string;
    args: ModelArgs;
    sets?: Array<string>;
}