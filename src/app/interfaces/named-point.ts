import { Vector3 } from 'three';

export class NamedPoint extends Vector3 {
    name: string;
    idx?: number;
    title?: string;
    description?: string;
    group?: string;
    delta?: Vector3;

    constructor(x?: number, y?: number, z?: number, name?: string, idx?: number, group?: string) {
        super(x, y, z);
        this.name = name || "";
        this.idx = (idx >= 0 ? idx : -1);
        this.group = group || "default";
    }

    public applyDelta(): void {
        if (this.delta) {
            this.add(this.delta);
            this.delta = null;
        }
    }
}