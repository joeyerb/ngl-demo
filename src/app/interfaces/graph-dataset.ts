import { Vector3 } from 'three';
import { Graph } from '../interfaces/graph';
import { NamedPoint } from '../interfaces/named-point';

export class GraphDataset {
    name: string;
    points: Array<any>;
    connections: Array<Array<any>>;

    constructor(name: string, points?: Array<any>, connections?: Array<Array<any>>) {
        this.name = name;
        this.points = points || [];
        this.connections = connections || [];
    }

    /**
     * Convert GraphDataset to Graph
     * 
     * @param {GraphDataset} data 
     * @returns {Graph} 
     * 
     * @memberOf GraphDataset
     */
    public toGraph(): Graph {

        let vertices = this.points.map((p, idx) => {
            let point = p.map(pi => pi * 100);
            return new NamedPoint(point[0], point[1], point[2], "", idx);
        });

        let edges = this.connections.map(e => e.map(n => vertices[n - 1]));

        let graph: Graph = {
            vertices: vertices,
            edges: edges
        };

        return graph;
    }

};