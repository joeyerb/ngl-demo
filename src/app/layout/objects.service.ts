import { Injectable } from '@angular/core';
import { SphereGeometry, Mesh, MeshLambertMaterial, MeshBasicMaterial, Texture, Vector3, ArrowHelper, CylinderGeometry, Euler } from 'three';
import { NamedPoint } from '../interfaces/named-point';

@Injectable()
export class ObjectsService {

    private edgeVolumn: number = 1000000;

    constructor() { }

    /**
     * Create a sphere
     *
     * @param {Vector3} position
     * @param {number} radius
     * @param {string} color
     * @param {number} idx
     * @param {NamedPoint} vertex
     * @returns {Mesh}
     *
     * @memberof ObjectsService
     */
    public createSphere(position: Vector3, radius: number, color: string, idx: number, vertex: NamedPoint): Mesh {
        let sphere = new SphereGeometry(radius || 100, 18, 18);
        let material = new MeshLambertMaterial({
            color: color || 0x96f07d,
            // map: new Texture(this.createTexture(idx + 1 + "")),
            // wireframe: false,
        });
        // material.map.needsUpdate = true;

        let ball = new Mesh(sphere, material);
        ball.position.set(position.x, position.y, position.z);
        ball.name = "v" + idx;
        ball.userData = vertex;

        return ball;
    }

    /**
     * Create texture for object
     *
     * @private
     * @param {string} text
     * @returns {HTMLCanvasElement}
     *
     * @memberof ObjectsService
     */
    private createTexture(text: string): HTMLCanvasElement {
        let x = document.createElement("canvas");
        let xc = x.getContext("2d");
        x.width = x.height = 128;
        xc.shadowColor = "#00ffff";
        xc.shadowBlur = 7;
        xc.fillStyle = "orange";
        xc.font = "30pt arial bold";
        xc.fillText(text, 30, 64);
        return x;
    }

    /**
     * Create an edge
     *
     * @param {Vector3} point1
     * @param {Vector3} point2
     * @param {MeshLambertMaterial} material
     * @param {string} nameIdx
     * @returns {Mesh}
     *
     * @memberof ObjectsService
     */
    public createEdge(point1: Vector3, point2: Vector3, material: MeshLambertMaterial, nameIdx: string): Mesh {
        let direction = new Vector3().subVectors(point2, point1);
        let arrow = new ArrowHelper(direction.clone().normalize(), point1);

        // 圆柱体
        let r = Math.sqrt(this.edgeVolumn / direction.length());
        let edgeGeometry = new CylinderGeometry(r, r, direction.length(), 10, 4);

        material = material || new MeshLambertMaterial({
            color: 0xff574b
        });
        let edge = new Mesh(edgeGeometry, material);
        edge.name = "e" + nameIdx;

        // 旋转
        let rotation = new Euler().setFromQuaternion(arrow.quaternion);
        edge.rotation.set(rotation.x, rotation.y, rotation.z);

        // 位置
        let p = new Vector3().addVectors(point1, direction.multiplyScalar(0.5));
        edge.position.set(p.x, p.y, p.z);

        return edge;
    };

    /**
     * Update an edge
     *
     * @param {Mesh} edge
     * @param {Vector3} point1
     * @param {Vector3} point2
     * @param {string} [color]
     *
     * @memberof ObjectsService
     */
    public updateEdge(edge: Mesh, point1: Vector3, point2: Vector3, color?: string): void {
        let direction = new Vector3().subVectors(point2, point1);
        let arrow = new ArrowHelper(direction.clone().normalize(), point1);

        let r = Math.sqrt(this.edgeVolumn / direction.length());
        if (r > 60) { r = 0; }
        let edgeGeometry = new CylinderGeometry(r, r, direction.length(), 10, 4);

        edge.geometry = edgeGeometry;

        // 旋转
        let rotation = new Euler().setFromQuaternion(arrow.quaternion);
        edge.rotation.set(rotation.x, rotation.y, rotation.z);

        // 位置
        let p = new Vector3().addVectors(point1, direction.multiplyScalar(0.5));
        edge.position.set(p.x, p.y, p.z);
    }
}