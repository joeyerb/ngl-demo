import { Injectable } from '@angular/core';
import { Vector3, Scene } from 'three';
import { ObjectsService } from '../layout/objects.service';
import { ForceModel } from '../interfaces/force-model';
import { Graph } from '../interfaces/graph';

@Injectable()
export class RenderService {

    private model: ForceModel;
    private graph: Graph;
    private scene: any;

    /**
     * Creates an instance of RenderService.
     *
     * @param {ObjectsService} objectsService
     *
     * @memberof RenderService
     */
    constructor(private objectsService: ObjectsService) { }

    /**
     * Initialize with arguments
     *
     * @param {ForceModel} model
     * @param {Graph} graph
     * @param {Scene} scene
     *
     * @memberof RenderService
     */
    public init(model: ForceModel, graph: Graph, scene: Scene) {
        this.model = model;
        this.scene = scene;
        this.graph = graph;
    }

    /**
     * Initial rendering
     *
     * @param {boolean} [noEdge]
     *
     * @memberof RenderService
     */
    public render(noEdge?: boolean): void {
        // this.logData();

        let origin = this.calculateNewOrigin();

        // TODO move to DataReader
        let colorGrp = {
            default: this.getRandomColor(),
        };
        let color = colorGrp.default;
        if (!this.graph.groups) this.graph.groups = {};
        this.graph.groups['default'] = color;

        this.graph.vertices.forEach((vertex, idx) => {
            if (colorGrp[vertex.group]) {
                color = colorGrp[vertex.group];
            } else {
                color = this.getRandomColor();
                colorGrp[vertex.group] = color;
                this.graph.groups[vertex.group] = color;
            }
            let sphere = this.objectsService.createSphere(vertex.sub(origin), 100, color, idx, vertex);
            this.scene.add(sphere);
        });

        if (!noEdge) {
            this.graph.edges.forEach((ps, index) => {
                for (let i = 1; i < ps.length; i++) {
                    if (this.graph.edges[index][0]['idx'] < this.graph.edges[index][i]['idx'])
                        this.scene.add(this.objectsService.createEdge(ps[0], ps[i], null, index + "-" + i));
                }
            });
        }
    }

    /**
     * Update objects' position
     *
     * @memberof RenderService
     */
    public update(): void {

        let origin = this.calculateNewOrigin();

        this.scene.traverse(obj => {
            if (obj.name) {
                if (obj.name.indexOf('v') === 0) {
                    let idx = obj.name.substr(1);
                    let pos = this.graph.vertices[idx].sub(origin);
                    obj.position.set(pos.x, pos.y, pos.z);
                }
                else if (obj.name.indexOf('e') === 0) {
                    let idxs = obj.name.substr(1).split('-');
                    if (idxs.length > 1) {
                        let row = idxs[0], col = idxs[1];
                        let p0 = this.graph.edges[row][0];
                        let pN = this.graph.edges[row][col];
                        this.objectsService.updateEdge(obj, p0, pN, this.graph.groups[pN.group]);
                    }
                }
            }
        });
    }

    /**
     * Toggle edges visibility
     *
     * @memberof RenderService
     */
    public toggleEdges(): void {
        this.scene.traverse(obj => {
            if (obj.name && obj.name[0] === 'e') {
                obj.visible = !obj.visible;
            }
        });
    }

    /**
     * Balance (final layout)
     *
     * @param {number} [currentStep]
     *
     * @memberof RenderService
     */
    public balance(currentStep?: number): void {
        this.model.balance(this.graph, currentStep);
        this.update();
    }

    /**
     * Forward n steps
     *
     * @param {number} nSteps
     * @param {number} [currentStep]
     *
     * @memberof RenderService
     */
    public step(nSteps: number, currentStep?: number): void {
        this.model.step(this.graph, nSteps, currentStep);
        this.update();
    }

    /**
     * Calculate new center of all nodes
     *
     * @private
     * @returns {Vector3}
     *
     * @memberof RenderService
     */
    private calculateNewOrigin(): Vector3 {
        let xsum = 0, ysum = 0, zsum = 0,
            vcount = this.graph.vertices.length;
        this.graph.vertices.forEach((vertex, idx) => {
            xsum += vertex.x;
            ysum += vertex.y;
            zsum += vertex.z;
        });
        return new Vector3(xsum / vcount, ysum / vcount, zsum / vcount);
    }

    /**
     * Log graph to console
     *
     * @private
     *
     * @memberof RenderService
     */
    private logData(): void {
        this.graph.vertices.forEach(vertex => console.log(JSON.stringify(vertex)));
        this.graph.edges.forEach((row, ri) => row.forEach((p, pi) => console.log(ri, row[0].name || row[0].idx, "to", p.name || p.idx, p.distanceTo(row[0]))));
        //this.graph.vertices.forEach((v1, i) => this.graph.vertices.forEach((v2, j) => console.log(i, j, v2.distanceTo(v1))));
    }

    /**
     * Get random color
     *
     * @private
     * @returns {string}
     *
     * @memberof RenderService
     */
    private getRandomColor(): string {
        let letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
}