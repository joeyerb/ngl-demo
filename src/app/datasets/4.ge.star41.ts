import { GraphDataset } from '../interfaces/graph-dataset';

// Star 4 + 1
export let dataset: GraphDataset = new GraphDataset(
    'star',
    [
        [3, 2, 9],
        [8, 12, 3],
        [6, 4, 17],
        [13, 7, 1],
        [15, 11, 4],
        [8, 1, 18],
        [2, 22, 11],
        [9, 10, -3],
        [-4, 10, 3]
    ], [
        [1, 2, 4],
        [2, 1, 3, 5, 4, 6],
        [3, 2, 6],
        [4, 1, 5, 7, 2, 8],
        [5, 2, 4, 6, 8],
        [6, 3, 5, 9, 2, 8],
        [7, 4, 8],
        [8, 5, 7, 9, 4, 6],
        [9, 6, 8],
    ]
);
