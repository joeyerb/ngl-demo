import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { NamedPoint } from '../interfaces/named-point';
import { NamedLink } from '../interfaces/named-link';

import { DataReader } from './0.data.reader';
import { DataReaderInterface } from '../interfaces/reader.interface';

@Injectable()
export class CsvReader extends DataReader implements DataReaderInterface {

    /**
     * Creates an instance of CsvReader.
     * 
     * @memberof CsvReader
     */
    constructor(protected http: Http) {
        super(http);
    }

    public getData(data :string, callback :Function) :void {
        this.loadData(data);
        let graph = this.toGraph();
        callback(graph);
    }

    /**
     * Load nodes and edges
     * 
     * @private
     * @param {string} data 
     * @returns {void} 
     * 
     * @memberof CsvReader
     */
    private loadData(data :string) :void {
        this.points = [];

        let lines: string[] = data.split('\n');

        for (let i = 1, idx = 0; i < lines.length; i++) {
            let line = lines[i];
            if (line.indexOf(',') < 0) continue;
            let path: string = lines[i].split(',')[1];

            let point: NamedPoint = new NamedPoint(
                this.getRandomArbitrary(0, 5000),   // x
                this.getRandomArbitrary(0, 5000),   // y
                this.getRandomArbitrary(0, 5000),   // z
                path, idx);
            point.description = decodeURIComponent(path);

            let parentName = this.getParent(point.name);
            if (parentName)
                point.group = parentName;
            else
                point.group = 'default';

            this.points.push(point);
            idx += 1;
        }

        this.links = [];
        this.points.forEach((point, idx) => {
            let i = point.name.lastIndexOf('/');
            if (i < 0) return;

            let sourceName = point.name.substr(0, i);
            let sourcePoint = this.points.find(p => p.name == sourceName)
            if (!sourcePoint) return;

            let namedLink = {
                idx: idx,
                name: point.name,
                source: sourcePoint,
                target: point,
                sourceName: sourceName,
                targetName: point.name,
                weight: 1,
            };
            this.links.push(namedLink);
        });
    }

    private getParent(pointName) {
        let i = pointName.lastIndexOf('/');
        if (i > 0) {
            return pointName.substr(0, i);
        } else {
            return '';
        }
    }

    public loadResp(resp: any): void {
        this.loadData(resp.text());
    }
}
