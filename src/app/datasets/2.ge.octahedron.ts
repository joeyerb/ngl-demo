import { GraphDataset } from '../interfaces/graph-dataset';

// Regular Octahedron
export let dataset: GraphDataset = new GraphDataset(
    '888',
    [
        [3, 2, 9],
        [8, 12, 3],
        [6, 4, 17],
        [13, 7, 1],
        [15, 11, 4],
        [8, 1, 18]
    ], [
        [1, 2, 4, 6, 3],
        [2, 1, 3, 5, 4],
        [3, 2, 6, 1, 5],
        [4, 1, 5, 2, 6],
        [5, 2, 4, 6, 3],
        [6, 1, 3, 5, 4]
    ]
);
