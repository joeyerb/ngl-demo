import { GraphDataset } from '../interfaces/graph-dataset';

// P. Eades 1984, A Heuristic for Graph Drawing, Figure 1(a)
export let dataset: GraphDataset = new GraphDataset(
    '666',
    [
        [3, 2, 9],
        [8, 12, 3],
        [6, 4, 17],
        [13, 7, 1],
        [15, 11, 4],
        [8, 1, 18]
    ], [
        [1, 2, 4, 6],
        [2, 1, 3, 5],
        [3, 2, 4, 6],
        [4, 1, 3, 5],
        [5, 2, 4, 6],
        [6, 1, 3, 5]
    ]
);