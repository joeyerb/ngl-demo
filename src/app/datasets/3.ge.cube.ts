import { GraphDataset } from '../interfaces/graph-dataset';

// Cube
export let dataset: GraphDataset = new GraphDataset(
    'cube',
    [
        [3, 2, 9],
        [8, 12, 3],
        [6, 4, 17],
        [13, 7, 1],
        [15, 11, 4],
        [8, 1, 18],
        [2, 22, 11],
        [9, 10, -3]
    ], [
        [1, 2, 4, 5],
        [2, 1, 3, 6],
        [3, 2, 4, 7],
        [4, 1, 3, 8],
        [5, 1, 6, 8],
        [6, 2, 5, 7],
        [7, 3, 6, 8],
        [8, 4, 5, 7],
    ]
);
