import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { NamedPoint } from '../interfaces/named-point';
import { NamedLink } from '../interfaces/named-link';

import { DataReader } from './0.data.reader';
import { DataReaderInterface } from '../interfaces/reader.interface';

@Injectable()
export class TsvReader extends DataReader implements DataReaderInterface {

    constructor(protected http: Http) {
        super(http);
    }

    /**
     * Read data from local file
     * 
     * @param data plain text
     * @param callback 
     */
    public getData(data: string, callback: Function) :void {
        this.loadNodes(data);
        this.loadEdges(data);
        let graph = this.toGraph();
        callback(graph);
    }


    /**
     * Load nodes from plain text
     * 
     * @private
     * @param {*} nodes
     * 
     * @memberOf TsvReader
     */
    private loadNodes(data: string): void {
        this.points = [];
        let firstRowIgnored = false;
        let lines: string[] = data.split('\n');
        for (let i = 0, idx = 0; i < lines.length; i++) {
            let line = lines[i].replace(/^\s+/, '');
            if (line === '' || line[0] === '#') continue;

            // Ignore table head
            if (!firstRowIgnored) {
                firstRowIgnored = true;
                continue;
            }

            let names: string[] = line.split(/\t/);

            names.forEach((name, col) => {

                // Only the first 2 fields are nodes' id
                if (col > 1) return;

                let description = "";

                // If this row has 3 fields,
                // use the 3rd field value as target's description
                if (names.length === 3 && col === 1) {
                    description = names[2];
                }
                // If this row has 4 fields,
                // use the 3rd field value as source's description
                // and the 4th field value as target's description
                else if (names.length === 4) {
                    if (col === 0) {
                        description = names[2];
                    }
                    else if (col === 1) {
                        description = names[3];
                    }
                } 

                // If this node name alread exists,
                // update it's description if needed,
                // then continue to next node.
                if (this.points.findIndex(point => {
                    let found = false;
                    if (point.name === name) {
                        found = true;
                        if (description && !point.description) {
                            point.description = description;
                        }
                    }
                    return found;
                }) >= 0) return;

                // Else create a new node
                let point: NamedPoint = new NamedPoint(
                    this.getRandomArbitrary(0, 5000),   // x
                    this.getRandomArbitrary(0, 5000),   // y
                    this.getRandomArbitrary(0, 5000),   // z
                    name, idx);

                if (description) {
                    point.description = decodeURIComponent(description);
                }

                this.points.push(point);
                idx += 1;
            });
        }
    }

    /**
     * Load edges from plain text
     * 
     * @private
     * @param {*} edges 
     * 
     * @memberOf TsvReader
     */
    private loadEdges(data: string): void {
        let edges: string[] = data.split('\n');
        this.links = [];
        for (let i = 0, idx = 0; i < edges.length; i++) {
            let names = edges[i].replace(/^ */, '');
            if (names === '' || names[0] === '#') continue;

            let linkEnds = names.split(/[\s]+/);

            let sourcePoint = this.points.find(point => point.name === linkEnds[0]);
            let targetPoint = this.points.find(point => point.name === linkEnds[1]);
            if (!sourcePoint || !targetPoint) continue;

            let link: NamedLink = {
                idx: idx,
                source: sourcePoint,
                target: targetPoint,
                sourceName: linkEnds[0],
                targetName: linkEnds[1],
                description: decodeURIComponent(names),
                name: names,
                weight: 1,
            }
            this.links.push(link);
            idx += 1;
        }
    }

    public loadResp(resp: any): void {
        this.loadNodes(resp.text());
        this.loadEdges(resp.text());
    }
}