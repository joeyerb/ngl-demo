import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { NamedPoint } from '../interfaces/named-point';
import { NamedLink } from '../interfaces/named-link';

import { DataReader } from './0.data.reader';
import { DataReaderInterface } from '../interfaces/reader.interface';

interface JsonNode {
    id :string;
    name? :string;
    group? :string;
    description? :string;
}

interface JsonLink {
    source :string;
    target :string;
    value? :number;     // weight
    _source? :NamedPoint;
    _target? :NamedPoint;
}

interface JsonDataSet {
    nodes :Array<JsonNode>;
    links :Array<JsonLink>;
}

@Injectable()
export class JsonReader extends DataReader implements DataReaderInterface {

    /**
     * Creates an instance of JsonReader.
     * 
     * @memberof JsonReader
     */
    constructor(protected http: Http) {
        super(http);
    }

    public getData(data :string, callback :Function) :void {
        this.loadData(data);
        let graph = this.toGraph();
        callback(graph);
    }

    /**
     * Load nodes and edges
     * 
     * @private
     * @param {string} data 
     * @returns {void} 
     * 
     * @memberof JsonReader
     */
    private loadData(data :string) :void {
        let dataSet :JsonDataSet;
        try {
            dataSet = JSON.parse(data);
        } catch (e) {
            console.error(e);
            return;
        }

        this.points = dataSet.nodes.sort((a, b) => {
            let aIdx = dataSet.links.findIndex(link => link.source === a.id || link.target === a.id);
            let bIdx = dataSet.links.findIndex(link => link.source === b.id || link.target === b.id);
            if (aIdx && bIdx) {
                if (dataSet.links[aIdx].source !== a.id) {
                    aIdx += 0.5;
                }
                if (dataSet.links[bIdx].source !== b.id) {
                    bIdx += 0.5;
                }
            }
            return aIdx - bIdx;
        }).map((node, idx) => {
            let point: NamedPoint = new NamedPoint(
                this.getRandomArbitrary(0, 5000),   // x
                this.getRandomArbitrary(0, 5000),   // y
                this.getRandomArbitrary(0, 5000),   // z
                node.id, idx, node.group);
            if (node.description)
                point.description = decodeURIComponent(node.description);
            if (node.name)
                point.title = decodeURIComponent(node.name);
            return point;
        });

        this.links = dataSet.links.filter(link => {
            link._source = this.points.find(point => point.name === link.source);
            link._target = this.points.find(point => point.name === link.target);
            if (!link._source || !link._target) return false;
            return true;
        }).map((link, idx) => {
            return {
                idx: idx,
                name: link.source + '--' + link.target,
                source: link._source,
                target: link._target,
                sourceName: link.source,
                targetName: link.target,
                weight: link.value || 1,
            };
        });
    }

    public loadResp(resp: any): void {
        this.loadData(resp.text());
    }
}