import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { NamedPoint } from '../interfaces/named-point';
import { NamedLink } from '../interfaces/named-link';
import { Graph } from '../interfaces/graph';

@Injectable()
export class DataReader {

    constructor(http?: Http) {
        if (http) {
            this.http = http;
        }
    }

    protected http: Http;
    protected points: Array<NamedPoint> = [];
    protected links: Array<NamedLink> = [];

    /**
     * Get data
     *
     * @param data: plain text
     * @param callback
     */
    public getData(data: string, callback: Function) :void {
        // virtual
    }

    /**
     * Get dataset from url
     *
     * @memberOf TsvReader
     */
    public getHttpData(url: string, callback: Function) {
        this.fetchData(url, callback);
    }

    /**
     * Fetch data
     *
     * @protected
     * @param url
     * @param callback
     */
    protected fetchData(url: string, callback: Function) {
        url = url || 'http://local.test/data/edges1.tsv';
        let promiseE = this.http.get(url).toPromise()
            .then(this.loadResp.bind(this)).then(() => {
                let graph = this.toGraph();
                callback(graph);
            });
    }

    /**
     * Load response data
     *
     * @virtual
     * @protected
     * @param {*} resp
     *
     * @memberof DataReader
     */
    protected loadResp(resp: any): void {
        return;
    }


    /**
     * Get file list
     *
     * @param {Function} callback
     * @param {string} [idxUrl]
     *
     * @memberof DataReader
     */
    public getIndex(callback: Function, idxUrl?: string): void {
        idxUrl = idxUrl || './datasets/index.html';
        this.http.get(idxUrl).toPromise()
            .then(this.indexFiles)
            .then(fileNames => callback(fileNames));
    }

    /**
     * Extract file names from nginx directory listing
     *
     * @private
     * @param {*} resp
     * @returns {Array<string>}
     *
     * @memberof DataReader
     */
    private indexFiles(resp: any): Array<string> {
        let fileNames = [];
        let lines: string[] = resp.text().split('\n');
        lines.forEach(line => {
            if (line.indexOf('href=') > 0 && line.indexOf('../') < 0) {
                let begin = line.indexOf('">');
                let end = line.indexOf('</a>');
                if (begin > 0 && end > 0) {
                    let fileName = line.substring(begin + 2, end);
                    if (fileName.endsWith('.json') || fileName.endsWith('.tsv') ||
                            fileName.endsWith('csv')) {
                        fileNames.push(fileName);
                    }
                }
            }
        });
        return fileNames;
    }

    /**
     * Convert to Graph
     *
     * @returns {Graph}
     *
     * @memberOf DataReader
     */
    protected toGraph(): Graph {
        let edges = [];
        this.links.forEach(link => {
            let rowOfSource = edges.find(points => points[0] === link.source);
            if (rowOfSource && !rowOfSource.find(point => point === link.target)) {
                rowOfSource.push(link.target);
            } else {
                edges.push([link.source, link.target]);
            }
            let rowOfTarget = edges.find(points => points[0] === link.target);
            if (rowOfTarget && !rowOfTarget.find(point => point === link.source)) {
                rowOfTarget.push(link.source);
            } else {
                edges.push([link.target, link.source]);
            }
        });
        let dataSet: Graph = {
            vertices: this.points,
            edges: edges,
        }
        return dataSet;
    }

    /**
     * Get a random integer in [min, max)
     *
     * @private
     * @param {number} min
     * @param {number} max
     * @returns {number}
     *
     * @memberOf DataReader
     */
    protected getRandomArbitrary(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}
