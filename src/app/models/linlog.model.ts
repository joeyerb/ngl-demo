import { Injectable } from '@angular/core';
import { Vector3 } from 'three';
import { ForceModel } from '../interfaces/force-model';
import { ModelArgs } from '../interfaces/model-args';
import { Graph } from '../interfaces/graph';
import { BaseModel } from './base.model';

@Injectable()
export class LinLogModel extends BaseModel implements ForceModel {

    /**
     * LinLog模型參數
     * 
     * @type {ModelArgs}
     * @memberof LinLogModel
     */
    args: ModelArgs = {
        attr: 0.0000033333, // 引力参数
        repu: 10000,        // 斥力参数
        slen: 0,            // 弹簧长度
        mlen: 10,           // 移动参数
        total: 50,          // 平衡次数
        time1: 0.5,         // r-PloyLog t1
        time2: 0.9,         // r-PloyLog t2
        rstart: 3,          // r-PloyLog r
    };

    /**
     * Creates an instance of LinLogModel.
     *
     * @memberOf LinLogModel
     */
    constructor() {
        super();
    }


    /**
     * Attractive force (r = 1; f = attr)
     *
     * @private
     * @param {Vector3} point1
     * @param {Vector3} point2
     * @param {number} attr
     * @returns {Vector3}
     * 
     * @memberOf LinLogModel
     */
    private forceA1(point1: Vector3, point2: Vector3, attr: number): Vector3 {
        this.args.rstart = 1;
        return this.forceA(point1, point2, attr, 0)
    }


    /**
     * Attractive force (r >= 1)
     *
     * @private
     * @param {Vector3} point1
     * @param {Vector3} point2
     * @param {number} attr
     * @param {number} slen
     * @returns {Vector3}
     *
     * @memberOf LinLogModel
     */
    private forceA(point1: Vector3, point2: Vector3, attr: number, slen: number): Vector3 {
        this.args.attr = attr || this.args.attr;
        this.args.slen = slen || this.args.slen;

        let v12 = new Vector3().subVectors(point2, point1);
        let d = point1.distanceTo(point2);

        // t < t1
        let r = this.args.rstart;
        // t >= t2
        if (this.currentStep / this.args.total >= this.args.time2) {
            r = 1;
        } else if (this.currentStep / this.args.total >= this.args.time1) {
            // t1 =< t < t2: r = a * rStart + 1 * (1-a); a = (t2*M - m) / (t2*M - t1*M);
            let a = (this.args.time2 * this.args.total - this.currentStep) / (this.args.total * (this.args.time2 - this.args.time1));
            r = ~~(this.args.rstart * a + (1 - a) * 1 + 0.5);
        }

        if (r < 2) {
            // Return: attr * (v12 / d)
            return v12.multiplyScalar(this.args.attr * this.args.scale / d);
        } else {
            // Return: attr * r * (d - slen) ^ (r-1) * (v12 / d)
            let er_1 = this.loop(r - 1, 1, (n, result) => result * (d - this.args.slen));
            return v12.multiplyScalar(this.args.attr * r * er_1 / d);
        }
    }


    /**
     * Repulsive force (r >= 1, g = repu / d)
     * 
     * @private
     * @param {Vector3} point1 
     * @param {Vector3} point3 
     * @param {number} repu 
     * @returns {Vector3} 
     * 
     * @memberOf LinLogModel
     */
    private forceR(point1: Vector3, point3: Vector3, repu: number): Vector3 {
        this.args.repu = repu || this.args.repu;
        let v31 = new Vector3().subVectors(point1, point3);
        let d = point1.distanceTo(point3);
        return v31.multiplyScalar(this.args.repu / d / d);
    }


    /**
     * Net force of vertex n
     * 
     * @protected
     * @param {number} n 
     * @param {Graph} data 
     * @returns {Vector3} 
     * 
     * @memberOf LinLogModel
     */
    protected netForce(n: number, data: Graph): Vector3 {
        let pointN = data.vertices[n];

        let rowN = data.edges.find(points => points[0] === pointN)
        let adjacentPoints = rowN.slice(1); // remove self
        let netForceA = adjacentPoints.reduce((result, px) => result.add(this.forceA(pointN, px, null, null)), new Vector3(0, 0, 0));

        let nonadjacentPoints = data.vertices.filter(px => adjacentPoints.findIndex(pa => px === pa) < 0 && px !== pointN);
        let netForceR = nonadjacentPoints.reduce((result, px) => result.add(this.forceR(pointN, px, null)), new Vector3(0, 0, 0));

        return new Vector3().addVectors(netForceA, netForceR);
    }


    /**
     * Current energy
     * 
     * @param {Graph} data 
     * @returns {number}
     * 
     * @memberOf LinLogModel
     */
    public energy(data: Graph): number {
        let that = this;
        let r = 1;

        let e_fa = function(v1: Vector3, v2: Vector3): number {
            let d = v1.distanceTo(v2);
            return that.loop(r, 1, (n, result) => result * (d - that.args.mlen));
        }

        let e_fr = function(v1: Vector3, v2: Vector3): number {
            return Math.log(v1.distanceTo(v2));
        }

        return this.calculateEnergy(data, e_fa, e_fr);
    }
}