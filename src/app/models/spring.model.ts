import { Injectable } from '@angular/core';
import { Vector3 } from 'three';
import { ForceModel } from '../interfaces/force-model';
import { ModelArgs } from '../interfaces/model-args';
import { BaseModel } from './base.model';
import { Graph } from '../interfaces/graph';

@Injectable()
export class SpringModel extends BaseModel implements ForceModel {

    /**
     * 彈簧模型參數
     * 
     * @type {ModelArgs}
     * @memberof SpringModel
     */
    args: ModelArgs = {
        attr: 2,        // 引力参数
        repu: 10,        // 斥力参数
        slen: 1000,     // 弹簧长度
        mlen: 100,      // 移动参数
        total: 100,     // 平衡次数
    };

    /**
     * Creates an instance of SpringModel.
     * 
     * @memberOf SpringModel
     */
    constructor() {
        super();
    }


    /**
     * Attractive force
     * 
     * @private
     * @param {Vector3} point1 
     * @param {Vector3} point2 
     * @param {number} attr 
     * @param {number} slen 
     * @returns {Vector3} 
     * 
     * @memberOf SpringModel
     */
    private forceA(point1: Vector3, point2: Vector3, attr: number, slen: number): Vector3 {
        this.args.attr = attr || this.args.attr;
        this.args.slen = slen || this.args.slen;

        let v12 = new Vector3().subVectors(point2, point1);
        let d = point1.distanceTo(point2);

        return v12.multiplyScalar(this.args.attr * Math.log(d / this.args.slen) / d);
    }


    /**
     * Repulsive force
     * 
     * @private
     * @param {Vector3} point1 
     * @param {Vector3} point3 
     * @param {number} repu 
     * @returns {Vector3} 
     * 
     * @memberOf SpringModel
     */
    private forceR(point1: Vector3, point3: Vector3, repu: number): Vector3 {
        this.args.repu = repu || this.args.repu;

        let v31 = new Vector3().subVectors(point1, point3);
        let d = point1.distanceTo(point3);

        let c = this.args.repu / (d * Math.sqrt(d));
        return v31.multiplyScalar(c);
    }


    /**
     * Net force of vertex n
     * 
     * @protected
     * @param {number} n 
     * @param {Graph} data 
     * @returns {Vector3}
     * 
     * @memberOf SpringModel
     */
    protected netForce(n: number, data: Graph): Vector3 {
        let pointN = data.vertices[n];

        let adjacentPoints = data.edges[n].slice(1); // remove self
        let netForceA = adjacentPoints.reduce((result, px) => result.add(this.forceA(pointN, px, null, null)), new Vector3(0, 0, 0));

        let nonadjacentPoints = data.vertices.filter(px => adjacentPoints.findIndex(pa => px === pa) < 0 && px !== pointN);
        let netForceR = nonadjacentPoints.reduce((result, px) => result.add(this.forceR(pointN, px, null)), new Vector3(0, 0, 0));

        return new Vector3().addVectors(netForceA, netForceR);
    }


    /**
     * Current energy
     * 
     * @param {Graph} data 
     * @returns {number} 
     * 
     * @memberOf SpringModel
     */
    public energy(data: Graph): number {
        let that = this;    // required, for e_fa & e_fr are callbacks

        let log_slen = Math.log(that.args.slen);
        let e_fa = function (v1: Vector3, v2: Vector3): number {
            let d = v1.distanceTo(v2);
            return that.args.attr * (Math.log(d) - 1 - log_slen);
        }

        let e_fr = function (v1: Vector3, v2: Vector3): number {
            let d = v1.distanceTo(v2);
            return 2 * that.args.repu * Math.sqrt(d);
        }

        return this.calculateEnergy(data, e_fa, e_fr);
    }
}