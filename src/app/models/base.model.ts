import { Vector3 } from 'three';
import { Graph } from '../interfaces/graph';
import { ModelArgs } from '../interfaces/model-args';

export abstract class BaseModel {

    /**
     * 模型參數
     * 
     * @type {ModelArgs}
     * @memberof BaseModel
     */
    args: ModelArgs = {
        attr: 1,        // 引力参数
        repu: 1,        // 斥力参数
        slen: 1000,     // 弹簧长度
        mlen: 100,      // 移动参数
        total: 100,     // 平衡次数
    };

    // currentStep
    protected currentStep: number = -1;

    /**
     * Creates an instance of BaseModel.
     * 
     * @memberOf BaseModel
     */
    constructor() { }

    /**
     * Set model arguments
     *
     * @param args
     */
    public setArgs(args: ModelArgs): void {
        this.args = args || this.args;
    }

    /**
     * Loop callback for n times
     * 
     * @private
     * @param {number} n 
     * @param {any} result 
     * @param {Function} callback
     * @returns {any}
     * 
     * @memberOf BaseModel
     */
    protected loop(n: number, result: any, callback: Function): any {
        return n > 1 ? this.loop(n-1, callback(n, result), callback) : callback(n, result);
    }

    /**
     * Net force of vertex n
     * 
     * @virtual
     * @protected
     * @param {number} n 
     * @param {Graph} data 
     * @returns {Vector3} 
     * 
     * @memberOf BaseModel
     */
    protected netForce(n: number, data: Graph): Vector3 {
        return new Vector3();
    }

    /**
     * Move vertex n
     * 
     * @param {number} n 
     * @param {Graph} data 
     * @param {number} args.mlen 
     * 
     * @memberOf BaseModel
     */
    protected calculateDelta(n: number, data: Graph, mlen: number): void {
        this.args.mlen = mlen || this.args.mlen;
        let pointN = data.vertices[n];
        let force = this.netForce(n, data);
        pointN.delta = force.multiplyScalar(this.args.mlen);
    }


    /**
     * Do balance
     * 
     * @param {Graph} data 
     * 
     * @memberOf BaseModel
     */
    public balance(data: Graph, currentStep?: number): void {
        if (currentStep && currentStep > this.args.total) {
            this.args.total *= 2;
        }
        this.step(data, this.args.total - currentStep, currentStep);
    }

    /**
     * Move all nodes n steps
     * 
     * @param {Graph} data 
     * 
     * @memberOf BaseModel
     */
    public step(data: Graph, nSteps?: number, currentStep?: number): void {
        nSteps = nSteps || 1;
        this.currentStep = currentStep || -1;
        this.loop(nSteps, null, () => {
            data.vertices.forEach((vertex, idx) => {
                this.calculateDelta(idx, data, null);
            });
            data.vertices.forEach((vertex, idx) => {
                vertex.applyDelta();
            });
            this.currentStep += 1;
        });
    }

    /**
     * Current energy
     * 
     * @virtual
     * @param {Graph} data 
     * @returns {number}
     * 
     * @memberOf BaseModel
     */
    public energy(data: Graph): number {
        return 0;
    }


    /**
     * Calculate energy
     * 
     * @protected
     * @param {Graph} data 
     * @param {Function} e_fa 
     * @param {Function} e_fr 
     * @returns {number} 
     * 
     * @memberOf BaseModel
     */
    protected calculateEnergy(data: Graph, e_fa: Function, e_fr: Function): number {
        return data.vertices.reduce((sum, vertex, i) => {
            return sum + this.getEnergy(i, vertex, data, e_fa, e_fr);
        }, 0);
    }

    private getEnergy(n: number, vertex: Vector3, data: Graph, e_fa: Function, e_fr: Function) {
            // Every edge of vertex i
            let adjacentVertices = data.edges[n].slice(1); // remove self
            let energyA = adjacentVertices.reduce((result, vx) => result + e_fa(vertex, vx), 0);

            // Every non-connected pair of vertex i
            let nonadjacentVertices = data.vertices.filter(px => adjacentVertices.findIndex(pa => px === pa) < 0 && px !== vertex);
            let energyR = nonadjacentVertices.reduce((result, vx) => result + e_fr(vertex, vx), 0);

            return energyA - energyR;
    }
}