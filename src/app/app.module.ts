import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@angular/material';

import { AppComponent } from './app.component';

import { ObjectsService } from './layout/objects.service';
import { RenderService } from './layout/render.service';

import { DataReader } from './datasets/0.data.reader';
import { TsvReader } from './datasets/6.tsv.reader';
import { JsonReader } from './datasets/7.json.reader';
import { CsvReader } from './datasets/8.csv.reader';

import { SpringModel } from './models/spring.model';
import { FRModel } from './models/fr.model';
import { LinLogModel } from './models/linlog.model';

import { CanvasComponent } from './demos/canvas.component';
import { TooltipComponent } from './demos/tooltip.component';
import { ForceModelDemo } from './demos/force-model.demo';
import { DataArgsService } from './demos/data-args';

@NgModule({
    declarations: [
        AppComponent,
        CanvasComponent,
        TooltipComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        BrowserAnimationsModule,
        MaterialModule
    ],
    providers: [
        SpringModel,
        FRModel,
        LinLogModel,
        ObjectsService,
        RenderService,
        DataReader,
        TsvReader,
        CsvReader,
        JsonReader,
        ForceModelDemo,
        DataArgsService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
